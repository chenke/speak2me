const videoFolder = 'static/videos'
const subtitleFolder = 'static'

var emotion_colors = {
    'angry': '#FF5733',
    'calm': '#00BFFF',
    'disgust': '#8B008B',
    'fearful': '#FF8C00',
    'happy': '#FFD700',
    'neutral': '#808080',
    'sad': '#6495ED',
    'surprise': '#FF69B4'
}

var emotion_emojis = {
    'angry': '😡',
    'calm': '😌',
    'disgust': '🤢',
    'fearful': '😨',
    'happy': '😊',
    'neutral': '😐',
    'sad': '😭',
    'surprise': '😯'
}

var videos = [
    { src: videoFolder + "/video_1" + "/video.mp4", subtitle: videoFolder + "/video_1" + "/subtitle.vtt" },
    { src: videoFolder + "/video_2" + "/video.mp4", subtitle: videoFolder + "/video_2" + "/subtitle.vtt" },
    { src: videoFolder + "/video_3" + "/video.mp4", subtitle: videoFolder + "/video_3" + "/subtitle.vtt" },
    { src: videoFolder + "/video_4" + "/video.mp4", subtitle: videoFolder + "/video_4" + "/subtitle.vtt" },
    { src: videoFolder + "/video_5" + "/video.mp4", subtitle: videoFolder + "/video_5" + "/subtitle.vtt" }
];

var playedVideos = [];

function playRandom() {
    var nextId = getRandomInt(0, videos.length - 1);

    if (!playNext(nextId)) {
        playRandom();
    }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


function playNext(id) {
    if (playedVideos.includes(id)) {
        return false;
    }
    folderName = videos[id].src.split("/")[2];
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/update_video?folder=' + folderName, true);
    xhr.send();
    setSubtitleText(0, id);

    var elemVideo = document.getElementById('video');

    elemVideo.setAttribute('src', videos[id].src);
    elemVideo.load();
    elemVideo.play();

    playedVideos.push(id);

    if (playedVideos.length === videos.length) {
        playedVideos = [];
    }

    return true;
}

async function updateTime() {
    var video = document.getElementById('video');
    // get current folder name from video source
    var videoSrc = video.src.split("/")[5];
    var time = video.currentTime;
    var id = videos.findIndex(x => x.src.split("/")[2] === videoSrc);
    setSubtitleText(time, id);
    $.ajax({
        type: 'POST',
        url: '/update_time',
        data: { 'time': time, 'video': videoSrc },
        success: function (response) {
            console.log(response);
            if ((response in emotion_colors)) {
                var color = emotion_colors[response];
                setSubtitleColor(color);
                setSubtitleEmoji(response);
            }
        },
        error: function (error) {
            console.log(error);
        }
    });
}

async function setSubtitleText(time, id) {
    const subtitleElement = document.getElementById('subtitle');
    const subtitleUrl = videos[id].subtitle;
    try {
        const response = await fetch(subtitleUrl);
        const vttText = await response.text();
        const cues = parseVttText(vttText);
        // Loop through each cue
        for (const element of cues) {
            const cue = element;
            if (time >= cue.startTime && time < cue.endTime) {
                subtitleElement.value = cue.text;
                resizeSubtitle();
                return;
            }
        }
    } catch (error) {
        console.error(error);
    }
}

function parseVttText(vttText) {
    const cues = [];
    const lines = vttText.split(/\r?\n/);
    let cue = null;
    for (let i = 0; i < lines.length; i++) {
        const line = lines[i];
        if (line.trim() === '') {
            // Empty line, skip
            continue;
        }
        if (cue === null) {
            // New cue, create a new object
            cue = {
                startTime: null,
                endTime: null,
                text: ''
            };
        }
        if (line.includes('-->')) {
            // Time range line, parse the start and end times
            const [startTime, endTime] = line.split('-->');
            cue.startTime = parseVttTime(startTime.trim());
            cue.endTime = parseVttTime(endTime.trim());
        } else {
            // Text line, add to the current cue's text
            cue.text += line.trim() + ' ';
        }
        if (i === lines.length - 1 || lines[i + 1].trim() === '') {
            // Last line or empty line, add the current cue to the cues array
            cues.push(cue);
            cue = null;
        }
    }
    return cues;
}

function parseVttTime(timeString) {
    const [hours, minutes, secondsAndMilliseconds] = timeString.split(':');
    const [seconds, milliseconds] = secondsAndMilliseconds.split('.');
    return (parseInt(hours) * 60 * 60 + parseInt(minutes) * 60 + parseInt(seconds)) + parseFloat('.' + milliseconds);
}


async function setSubtitleColor(desiredColor) {
    const subtitleTrack = document.getElementById('subtitle');
    subtitleTrack.style.color = desiredColor;
}

async function resizeSubtitle() {
    const subtitleElement = document.getElementById('subtitle');
    const subtitleContainerElement = document.getElementById('subtitle-container');
    subtitleElement.style.width = 'auto';
    subtitleElement.style.height = 'auto';
    subtitleElement.style.width = subtitleElement.scrollWidth + 'px';
    subtitleElement.style.height = subtitleElement.scrollHeight + 'px';
    subtitleContainerElement.style.height = subtitleElement.scrollHeight + 'px';
}

async function setSubtitleEmoji(emotion) {
    const emoji = emotion_emojis[emotion];
    const smiley1Element = document.getElementById('smiley-1');
    const smiley2Element = document.getElementById('smiley-2');
    smiley1Element.innerText = emoji;
    smiley2Element.innerText = emoji;
}

