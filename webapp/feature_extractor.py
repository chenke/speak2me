import os
from time import sleep
import numpy as np
import librosa
import soundfile as sf
import wav2vec

def wav_vector(filepath, callback=None):
    result = []
    wd = wav2vec.WavDecoder(filepath)
    with wd as data:
        for frame in data:
            if callback != None:
                frame = callback(frame)
            result.append(frame)
    result = np.array(result).flatten()
    result = np.delete(result, np.arange(0, result.size, 2))
    return result[np.linspace(0, result.size-1, 500, dtype=int)]


def noise(data):
    noise_amp = 0.035*np.random.uniform()*np.amax(data)
    data = data + noise_amp*np.random.normal()
    return data


def extract_features(path, data, sample_rate, callback=None, wav2vec=False):

    # # Extract features
    result = np.array([])

    data = librosa.util.normalize(data)
    if 'normalized.wav' in os.listdir():
        try:
            os.remove('normalized.wav')
        except:
            pass

    result = np.array([])
    if wav2vec:
        sf.write('normalized.wav', data, sample_rate)
        result = wav_vector('normalized.wav', callback)

    zcr = np.mean(librosa.feature.zero_crossing_rate(y=data).T, axis=0)
    result=np.hstack((result, zcr))

    # # Chroma_stft
    stft = np.abs(librosa.stft(data))
    chroma_stft = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
    result = np.hstack((result, chroma_stft))

    # MFCC
    mfcc = np.mean(librosa.feature.mfcc(y=data, sr=sample_rate).T, axis=0)
    result = np.hstack((result, mfcc))

    # Root Mean Square Value
    rms = np.mean(librosa.feature.rms(y=data).T, axis=0)
    result = np.hstack((result, rms))

    # # MelSpectogram
    mel = np.mean(librosa.feature.melspectrogram(y=data, sr=sample_rate).T, axis=0)
    result = np.hstack((result, mel))

    return result


def get_features(path):
    data, sample_rate = librosa.load(path, duration=2.5, offset=0.6)

    result = extract_features(path,data,sample_rate,None)

    res2 = extract_features(path,data,sample_rate,noise)
    result = np.vstack((result, res2))

    return result
