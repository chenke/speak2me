from flask import Flask, render_template, request, jsonify, send_from_directory
import wave
import math
import pydub
import webvtt
import os
from feature_extractor import get_features
import numpy as np
from keras.models import load_model


app = Flask(__name__)
# load the trained model
model = load_model('static/models/model.h5')
last_file = ''
vtt_file_path = ''
wav_file_path = ''
folder = ''

def split_wav_by_vtt(vtt_file, wav_file, output_directory, name):
    # Load VTT file
    captions = webvtt.read(vtt_file)

    # Open WAV file (if it doesn't exist, create it from the MP4 file)
    if not os.path.exists(wav_file):
        pydub.AudioSegment.from_file(wav_file.replace('.wav', '.mp4')).export(wav_file, format='wav')
    wav = wave.open(wav_file, 'rb')

    os.makedirs(output_directory, exist_ok=True)

    for _, caption in enumerate(captions):
        # Calculate the start and end time for the current caption
        start_time = caption.start_in_seconds
        end_time = caption.end_in_seconds

        # Calculate the start and end frame for the current caption
        start_frame = math.floor(start_time * wav.getframerate())
        end_frame = math.floor(end_time * wav.getframerate())

        # Set the position of the WAV file to the start frame
        wav.setpos(start_frame)

        # Read frames from start frame to end frame
        frames = wav.readframes(end_frame - start_frame)

        # Create a new WAV file for the current caption
        output_file = os.path.join(output_directory, f'{name}_{start_time:0.2f}_{end_time:0.2f}.wav')
        with wave.open(output_file, 'wb') as output_wav:
            output_wav.setparams(wav.getparams())
            output_wav.writeframes(frames)

    # Close the WAV file
    wav.close()

def randomize_video():
    ran_int = np.random.randint(1, 5)
    folder = f"video_{ran_int}"
    return folder


def init_video(folder):
    global vtt_file_path
    global wav_file_path
    global last_file
    vtt_file_path = f'static/videos/{folder}/subtitle.vtt'
    wav_file_path = f'static/videos/{folder}/video.wav'
    last_file = ''

    output_directory_path = f"static/videos/{folder}/slices"
    split_wav_by_vtt(vtt_file_path, wav_file_path, output_directory_path, "video")

@app.route('/update_model')
def update_model():
    model_file = request.args.get('model')
    print("Model file: ", model_file)
    model = load_model(f'static/models/{model_file}.h5')
    # Update the model file here
    return jsonify({'message': 'Model updated successfully'})

@app.route('/update_video')
def update_video():
    global folder
    folder = request.args.get('folder')
    init_video(folder)
    # Update the model file here
    return jsonify({'message': 'Model updated successfully'})

@app.route('/speak2me')
def speak2me():
    global folder
    folder = randomize_video()
    init_video(folder)
    return render_template('speak2me.html')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/update_time', methods=['POST'])
def update_time():
    time = float(request.form['time'])
    prediction = ""
    global last_file
    global folder
    # find the wav file that contains the time in slices
    for file in os.listdir(f'static/videos/{folder}/slices'):
        if file.endswith(".wav") and file.startswith("video"):
            start_time = float(file.split('_')[1])
            end_time = float(file.split('_')[2].split('.')[0])
            if time+0.1 >= start_time and time <= end_time and last_file != file:
                last_file = file
                features = get_features(f"static/videos/{folder}/slices/"+file)[0]
                features = np.expand_dims(features, axis=0)
                # pass features to your trained model for prediction
                prediction = model.predict(features)
                # convert prediction to string
                emotion = ['angry', 'calm', 'disgust', 'fearful', 'happy', 'neutral', 'sad', 'surprise']
                print(prediction.tolist()[0])
                print(np.argmax(prediction))
                prediction = emotion[np.argmax(prediction)]
                print(prediction)
    return prediction

if __name__ == '__main__':
    app.run(debug=True)
