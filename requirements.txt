Flask>=2.3.2
ipython>=8.4.0
keras>=2.12.0
librosa>=0.10.0.post2
matplotlib>=3.5.2
numpy>=1.23.1
pandas>=1.4.3
pyaudio>=0.2.13
wav2vec>=1.0.1
scikit_learn>=1.1.2
seaborn>=0.11.2
tensorflow>=2.12.0
SpeechRecognition>=3.10.0
pydub>=0.25.1
webvtt-py>=0.4.6