# Speak2Me
speak2me is a webapp that uses a trained model to interpret emotion from audio input and ovelay colours and emoticons on the subtitles.

## Description

This repository contains the dataset the model was trained on, the preprocessing code, and the training of the model.
In addition there is a notebook with visualizations of the data and the webapp that uses the trained model for new data.

## Usage
The model can built using the following steps:
1. Clone this repository
2. Move into the repositories directory
4. If you do not have it install virtualenv for python using `pip install python3-virtualenv`
5. Run `virtualenv env`
6. Run `source env/bin/activate`on linux/mac or `.\env\Scripts\activate` on windows
7. Install the requirements using `pip install -r requirements.txt`
8. Unify the dataset using the notebook unify_dataset.ipynb, this creates a data.csv file in the data folder
   Make sure to select the virtual python environment.
9. Run the notebook preprocess.ipynb, this creates a features.csv file in the data folder
10. Run the notebook train_model.ipynb, this creates a model h5 file in the models folder and shows visualizations and metrics of the model

The webapp can be run using the following steps:
1. run the webapp using `python app.py`
2. open the webapp in your browser using the url `http://localhost:5000/`
3. start the video to view the augmented subtitles


## Contributing


## Authors and acknowledgment
Kevin Chen
Leila Masimli
Terence Stenvold

## License
MIT License

## Sources
Our project is built on the following sources: [Kaggle](https://www.kaggle.com/code/shivamburnwal/speech-emotion-recognition/notebook)
